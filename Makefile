SCP = scp
CP = cp
DOCKER = docker
RUN = run
START = start
STOP = stop
ECHO = echo
PWD = $(shell pwd)
SERVER = orthanc
PYTHON = python3
BASH_IMPORT = source

APACHE_SERVER_HTML = /var/www/html
DOC_LOCATION = docs/*

PCCOMP_CRAWLER = $(PWD)/src/pccomponentes/standalone_crawler_pccomp.py
PCCOMP_CRAWLER_ALL = --crawl-all

MMARKT_CRAWLER = $(PWD)/src/mediamarkt/standalone_crawler_mediamarkt.py
MMARKT_NAME = mediamakrt
MMARKT_CRAWLER_ALL = --crawl-all
MMARKT_DB_CONF = --delete-prev --database postgres

SIMULATOR = $(PWD)/src/crawlers/data_simulator/simulator.py
SIMULATOR_COMM_ARGS = -n 400 -D 25 -d elasticsearch -C displays -c
SIMULATOR_CLEAN_ARGS =  -d elasticsearch -C displays --clean-db

INSTALLATION_SCRIPT = bin/install

BASH_PYTHON_PATH="export PYTHONPATH=${PWD}/src/common:${PWD}/src/mediamarkt:${PWD}/src/pccomponentes"
export PYTHONPATH=$(PWD)/src/common:$(PWD)/src/mediamarkt:$(PWD)/src/pccomponentes
export OUTPUTS_PATH=$(PWD)/outputs
export CHROME_DRIVER=$(PWD)/src/chromedriver
export POSTGRES_CONF=$(PWD)/config/postgres.conf
export ELASTICSEARCH_CONF=$(PWD)/config/elasticsearch.conf

ELASTICSERACH_DOCKER = docker.elastic.co/elasticsearch/elasticsearch:7.7.1
ELASTICSERACH_PARAMS = -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node"

POSTGRES_DOCKER = postgres
POSTGRES_PORT = 5433
POSTGRES_PASSWORD = Crawler2020?
POSTGRES_PARAMS = -h postgresdb_local -e POSTGRES_PASSWORD=$(POSTGRES_PASSWORD) -p $(POSTGRES_PORT):5432

create_docker_elasticsearch:
	$(DOCKER) $(RUN) $(ELASTICSERACH_PARAMS) $(ELASTICSERACH_DOCKER)
	@echo Done

create_docker_postgres:
	$(DOCKER) $(RUN) $(POSTGRES_PARAMS) -d $(POSTGRES_DOCKER)
	@echo Done

crawl_pccomponentes:
	$(PYTHON) $(PCCOMP_CRAWLER) $(PCCOMP_CRAWLER_ALL)
	@echo Done

crawl_mediamarkt:
	$(PYTHON) $(MMARKT_CRAWLER) $(MMARKT_CRAWLER_ALL) $(MMARKT_DB_CONF)
	@echo Done

run_sim_mediamarkt:
	$(PYTHON) $(SIMULATOR) $(SIMULATOR_COMM_ARGS) $(MMARKT_NAME)
	@echo Done

clean_elasticsearch:
	$(PYTHON) $(SIMULATOR) $(SIMULATOR_CLEAN_ARGS)
	@echo Done

deploy_doc:
	sudo $(CP) -r $(DOC_LOCATION) $(APACHE_SERVER_HTML)
	@echo Done

help:
	@echo test

install:
	./$(INSTALLATION_SCRIPT)
