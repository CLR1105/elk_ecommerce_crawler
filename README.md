# README

## Clone the repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).

To clone the repository you could execute the next command in system terminal:

    git clone https://CLR1105@bitbucket.org/CLR1105/elk_ecommerce_crawler.git

## Configure PYTHONPATH

In the repository there are three folders that should be added to python path. These folders contains the crawlers code
- src/common
- mediamarkt
- pccomponentes

To add the folders to PYTHONPATH, go to root dir of the repository and execute:
    
    root_dir=$(pwd)
    export PYTHONPATH=${root_dir}/src/common:${root_dir}/src/mediamarkt:${root_dir}/src/pccomponentes

This command only works in a unique bash shell. To make it work in each shell session, it could be added to ~/.bashrc or ~/.bash_profile but the added path should be the absolute paths:

    export PYTHONPATH=<repository_root_path>/src/common:<repository_root_path>/src/mediamarkt:<repository_root_path>/src/pccomponentes


## Installation guide

### Installation script

There are a installation script located in bin folder:

    bin/install

This script install the needed packages to run the crawlers, included python libraries. Also install docker and configure some functions located in src/bash_functions to manage the dockers of:

- postgres
- kibana
- elasticsearch

### Packages installation
To install the databases manually and other packages there are a installation guide in docs folder.

- docs/index.html: index of the docs content and project guide. This contains the same information than this README file.
- docs/Docker.html: docker installation guide.
- docs/Elasticsearch.html: Elasticsearch installation guide by docker or system package.
- docs/Kibana.html: Kibana installation guide by docker or system package.
- docs/MongoDB.html: small guide of MongoDB to install and configure it.
- docs/PostgresSQL.html: postgreSQL installation and configuration guide by system package.
- docs/PythonDocGuide.html: some demos examples.


### Python libraries:

To install the needed python libraries manually execute the next command in shell:

    sudo apt-get install python3-pip libpq-dev python-dev
    sudo pip3 install Scrapy selenium psycopg2 SQLAlchemy SQLAlchemy-Utils pandas numpy
    python3 -m pip install elasticsearch


## Run crawlers

There are two crawlers / scrapers:

- standalone_crawler_mediamarkt.py
- standalone_crawler_pccomp.py

To run both, there are some args:

    --version             show program's version number and exit
    -h, --help            show this help message and exit
    -s URL, --crawl-single-page=URL
                        Crawl a simple page
    -n NUM_PAGES, --crawl-some-pages=NUM_PAGES
                        Crawl a specified number of pages
    -c, --crawl-all       Crawl all displays
    -o OUTPUT_FILE, --output=OUTPUT_FILE
                        Run refresh errors
    -d DATABASE, --database=DATABASE
                        Database to store de data. Options: postgres, sqlite,
                        elasticsearch.
    --delete-prev         Delete previous data of the database
  
Usage: python3 standalone_crawler_pccomp.py [args]

Examples:

    python3 standalone_crawler_mediamarkt.py --crawl-all --delete-prev --database postgres
    python3 standalone_crawler_mediamarkt.py --crawl-single-page https://www.mediamarkt.es/es/product/_monitor-samsung-s24r350fhu-23-8-led-full-hd-1920x1080-freesync-250-cd-m%C2%B2-5-ms-178%C2%BA-hdmi-plata-1473718.html
    python3 standalone_crawler_mediamarkt.py --delete-prev --database postgres
    
    python3 standalone_crawler_pccomp.py --crawl-all
    python3 standalone_crawler_pccomp.py --crawl-single-page https://www.pccomponentes.com/msi-optix-g271-27-led-ips-fullhd-144hz-freesync

## Run data simulator

To run the simulator, there are some args:
    
    --version             show program's version number and exit
    -h, --help            show this help message and exit
    -n NUM_DAYS, --num-days=NUM_DAYS
                        nom days to generate data
    -D NUM_DISPLAYS, --num-displays=NUM_DISPLAYS
                        Generate data for x displays
    -c CRAWLER_NAME, --crawler-name=CRAWLER_NAME
                        Crawler name to simulate
    -d DATABASE, --database=DATABASE
                        database to store: sqlite, postgres, elasticsearch
    -C COLLECTION, --collection=COLLECTION
                        collection or table to store in database
    --clean-db            clean the database entries
    --delete-index        delete de index

The mandatory args are:

    --num-days
    --num-displays

Usage: python3 simulator.py [args]

Examples:

    python3 simulator.py -n 100 -D 200 -c NoldorMarkt
    python3 simulator.py -n 10 -D 20 -c EdainComponentes -d elasticsearch -c simulated_displays
    

## 5. ELK crawler Sandbox
If you do not want to install the packages in your system, you can download a sandbox that has a sandbox has been configured based on a Ubuntu Virtual Machine of VirtualBox. This sandbox could be download from the next url:

 - https://mega.nz/file/gMt1kLDT#YuYlJWcXs3H_Yn2Nn9OcOT8h1OWClhEpfUabW5Fi_zI

Login credentials of sandbox:

 - user: elk_user
 - password: Palantir2020?

The root location of the project is: $HOME/elk_ecommerce_crawler. The source code of the project is located in that path.

If you start the sandbox, you can go to localhost:8080 in your web browser and you will access to source project documentation.


## Contact

If you have any doubt, you could send an email to christian.lopezre@gmail.com