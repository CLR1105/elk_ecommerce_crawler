#!/usr/bin/python3
# -*- coding: utf-8; mode python -*-
"""
Database functions to manage the connections and the operations
"""

import os
import json
import traceback
from typing import List
from sqlalchemy import create_engine
from sqlalchemy_utils import create_database, database_exists
from sqlalchemy.orm import sessionmaker
from elasticsearch import Elasticsearch
from database.models import Base, Display, Ecommerce
from constants import POSTGRES_CONF, SQLITE_PATH, ELASTICSEARCH_CONF

__database = None
__connection_conf = None
__driver = None
__session = None

# sql alchemy drivers and other default drivers definition
PYTHON_DB_DRIVERS = {
    "postgres": "postgresql",
    "sqlite": "sqlite",
    "elasticsearch": "elasticsearch"
}


def load_configuration(db: str):
    """
    Load database configuration

    Args:
        db: database name
    """
    global __database
    global __connection_conf
    global __driver

    __database = db
    __driver = PYTHON_DB_DRIVERS.get(db)

    if db == 'postgres':
        db_conf = POSTGRES_CONF
    elif db == 'sqlite':
        __connection_conf = "sqlite"
        if not os.path.isdir(SQLITE_PATH):
            os.mkdir(SQLITE_PATH)
        return
    elif db == 'elasticsearch':
        db_conf = ELASTICSEARCH_CONF

    with open(db_conf, 'r') as database_conf:
        __connection_conf = json.load(database_conf)


def get_connection_string(db: str, **kwargs):
    """
    This function make a connection string to connect to database with
    sqlalchemy

        example:
            dialect+driver://username:password@host:port/database

    Args:
        db: atabase name
        kwargs: optional args

    Returns:
        connection string

    """

    global __database
    global __connection_conf
    global __driver

    if not __database or not __connection_conf or not __driver:
        load_configuration(db)

    if __driver == 'sqlite':
        sqlite_name = kwargs.get("sqlite_name", "crawlers")
        return f"{__driver}:///{os.path.join(SQLITE_PATH, sqlite_name)}"
    elif __driver == 'elasticsearch':
        host = __connection_conf.get("hostname")
        port = __connection_conf.get("port")
        return f"{host}:{port}"
    else:
        user = __connection_conf.get("user")
        password = __connection_conf.get("password")
        hostname = __connection_conf.get("hostname")
        port = __connection_conf.get("port")
        database = __connection_conf.get("database")

        return f"{__driver}://{user}:{password}@{hostname}:{port}/{database}"


def get_db_session(connection_conf: str) -> sessionmaker:
    """
    Get the connection session to database sql

    Args:
        connection_conf: connection configuration string

    Returns:
        session of the connection to database
    """
    global __session

    if __session:
        return __session

    engine = create_engine(connection_conf)

    if not database_exists(engine.url):
        create_database(engine.url)

    Base.metadata.create_all(bind=engine)
    session = sessionmaker(bind=engine)

    return session()


def store_sql(models: List, database: str, **kwargs):
    """
    Store display list into SQL database

    Args:
        models: sqlalchemy models list to store
        database: sql database type to store
        **kwargs: optional args
    """
    connection_conf = get_connection_string(db=database, **kwargs)

    db_session = get_db_session(connection_conf)
    try:
        for model in models:
            db_session.add(model)

        db_session.commit()
    except Exception as e:
        db_session.rollback()
        print(e)
        print(f"Error storing data")
        print(traceback.format_exc())

    db_session.close()


def delete_sql_data(database: str):
    """
    Delete data from sql database except SQLite

    Args:
        database: database to delete data
    """
    connection_conf = get_connection_string(db=database)

    engine = create_engine(connection_conf)
    delete_query = f"delete from {Display.__tablename__}; " \
                   f"delete from {Ecommerce.__tablename__}"

    with engine.connect() as conn:
        conn.execute(delete_query)


def store_elasticsearch(data: List, es_index: str):
    """
    Store display list into elasticsearch database

    Args:
        displays: display list to store
        es_index: display collection to store in elasticsearch
    """
    es = Elasticsearch(hosts=[get_connection_string(db="elasticsearch")])

    for model in data:
        if isinstance(model, Display):
            if not model.is_valid():
                continue
            model = model.to_json()
        else:
            tmp_display = Display()
            tmp_display.json_2_model(model)
            if not tmp_display.is_valid():
                continue
        try:
            es.index(index=es_index, body=model)
        except Exception as e:
            print("display that raise the error: ", model)
            print(e)


def drop_elasticsearch_index(es_index: str):
    """
    Drop elasticsearch index

    Args:
        es_index: display collection to store in elasticsearch
    """
    es = Elasticsearch(hosts=[get_connection_string(db="elasticsearch")])
    es.indices.delete(index=es_index, ignore=[400, 404])


def delete_elasticsearch_index(es_index: str):
    """
    Delete all data of elasticsearch index

    Args:
        es_index: display collection to store in elasticsearch
    """
    es = Elasticsearch(hosts=[get_connection_string(db="elasticsearch")])
    es.delete_by_query(index=es_index, body={"query": {"match_all": {}}})

# TODO: add mongodb
"""
def store_mongo(displays, collection):
    connection = MongoClient(
        host=const.MONGO_CLIENT_DB_HOST,
        port=const.MONGO_CLIENT_DB_PORT,
        username=const.MONGO_CLIENT_DB_USERNAME,
        password=const.MONGO_CLIENT_DB_PASSWORD,
        authSource=const.MONGO_CLIENT_DB_NAME
    )
    db = connection[const.MONGO_CLIENT_DB_NAME]
    collection = db[collection]

    for display in displays:
        collection.insert(display.document_model)
"""