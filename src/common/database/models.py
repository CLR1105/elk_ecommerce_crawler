from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Float, ForeignKey, JSON

Base = declarative_base()


class Ecommerce(Base):
    __tablename__ = "e_commerce"

    db_id = Column(Integer, primary_key=True, unique=True, autoincrement=True)
    name = Column(String(35))
    displays = relationship("Display", back_populates="e_commerce")

    def __str__(self):
        return f"[Database id: {self.db_id}, name: {self.name}]"


class Display(Base):
    __tablename__ = "display"

    db_id = Column(Integer, primary_key=True, unique=True, autoincrement=True)
    e_commerce_id = Column(Integer, ForeignKey('e_commerce.db_id'))
    title = Column(String(250))
    price = Column(Float)
    brand = Column(String(25))
    p_n = Column(String(25))
    model = Column(String(50))
    size = Column(Float)
    image_quality = Column(String(25))
    resolution = Column(String(10))
    resolution_values = Column(JSON)
    response_time = Column(Float)
    refresh_speed = Column(Float)
    url = Column(String(1000))
    e_commerce = relationship("Ecommerce", back_populates="displays")

    def to_json(self):
        return {
            "title": self.title,
            "price": self.price,
            "brand": self.brand,
            "p_n": self.p_n,
            "model": self.model,
            "size": self.size,
            "image_quality": self.image_quality,
            "resolution": self.resolution,
            "resolution_values": self.resolution_values,
            "response_time": self.response_time,
            "refresh_speed": self.refresh_speed,
            "url": self.url,
            "e_commerce": self.e_commerce.name
        }

    def json_2_model(self, json):
        if not json.get("name"):
            self.title = json.get("title")
        else:
            self.title = json.get("name")
        self.price = json.get("price")
        self.brand = json.get("brand")
        self.p_n = json.get("p_n")
        self.model = json.get("model")
        self.size = json.get("size")
        self.image_quality = json.get("image_quality")
        self.resolution = json.get("resolution")
        self.resolution_values = json.get("resolution_values")
        self.response_time = json.get("response_time")
        self.refresh_speed = json.get("refresh_speed")
        self.url = json.get("url")
        self.e_commerce = Ecommerce(name=json.get("e_commerce"))

    def is_valid(self):
        if not self.price:
            return False
        if not isinstance(self.price, float):
            return False

        if not self.brand:
            return False
        if not isinstance(self.brand, str):
            return False

        if not self.model:
            return False
        if not isinstance(self.model, str):
            return False

        if not self.size:
            return False
        if not isinstance(self.size, float):
            return False

        if not self.resolution:
            return False
        if not isinstance(self.resolution, str):
            return False

        if not self.image_quality:
            return False
        if not isinstance(self.image_quality, str):
            return False

        if not self.response_time:
            return False
        if not isinstance(self.response_time, float):
            return False

        if not self.refresh_speed:
            return False
        if not isinstance(self.refresh_speed, float):
            return False

        return True

    def clone(self):
        return Display(
            title=self.title,
            price=self.price,
            brand=self.brand,
            p_n=self.p_n,
            model=self.model,
            size=self.size,
            image_quality=self.image_quality,
            resolution=self.resolution,
            resolution_value=self.resolution_value,
            response_time=self.response_time,
            refresh_speed=self.refresh_speed,
            url=self.url,
            e_commerce=self.e_commerce,
        )





