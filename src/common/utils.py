#!/usr/bin/python3
# -*- coding: utf-8; mode python -*-
"""
Common functions
"""
import os
import time
import json
import constants as common_const
from selenium import webdriver, common
from database.manager import get_db_session, get_connection_string
from database.models import Display, Ecommerce


def get_driver() -> webdriver:
    """
    Get selenium Chrome web driver

    Returns:
        Selenium Chrome web driver
    """
    driver = webdriver.Chrome(common_const.DRIVER_PATH)
    driver.maximize_window()
    return driver


def date_to_str(disp: Display) -> Display:
    """
    Convert datetime of display to string type

    Args:
        disp: display

    Returns:
        display
    """
    disp["date"] = str(disp.get("date"))
    return disp


def generate_output(data: list, data_type: str = None,
                    crawler: str = None, dest_file: str = None):
    """
    Generates a json output that contains the data that has been extracted

    Args:
         data: extracted data
         data_type: data type of extracted data: displays
         crawler: crawler that has extracted the data
         dest_file: output file
    """
    if not dest_file:
        output_filename = "%sCrawler_%s_%s.json" % (
            crawler, time.strftime("%Y%m%d%Hh%Mm%Ss"), data_type
        )

        if not os.path.isdir(common_const.OUTPUT_RESULTS_PATH):
            os.mkdir(common_const.OUTPUT_RESULTS_PATH)

        output_file_path = os.path.join(
            common_const.OUTPUT_RESULTS_PATH, crawler)

        if not os.path.isdir(output_file_path):
            os.mkdir(output_file_path)

        dest_file = os.path.join(output_file_path, output_filename)

    with open(dest_file, 'w') as output_file:
        if data_type != 'urls':
            if data and isinstance(data[0], Display):
                data = list(map(lambda disp: disp.to_json(), data))
            else:
                data = list(map(date_to_str, data))

        output_file.write(json.dumps(data, indent=4))


def get_ecommerce(ecommerce_name: str, database: str, **kwargs) -> Ecommerce:
    """
    Get ecommerce from database

    Args:
        ecommerce_name: ecommerce name
        database: database to get ecommerce, if it is None, then the Ecommerce
                  object is created as default
        kwargs: optional args

    Returns:
        Ecommerce
    """
    e_commerce = Ecommerce(name=ecommerce_name)

    if database and database != 'elasticsearch':
        connection_conf = get_connection_string(db=database, **kwargs)
        session = get_db_session(connection_conf)
        result = session.query(Ecommerce).filter(Ecommerce.name == ecommerce_name)
        for row in result:
            e_commerce = row

    return e_commerce
