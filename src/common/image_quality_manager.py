#!/usr/bin/python3
# -*- coding: utf-8; mode python -*-

"""
Functions to get the resolution from classification of image quality
"""
import json
from typing import Dict, Tuple
from constants import IMAGE_QUALITY_CONF_FILE


__resolution_classes = None
__equivalences_classes = None


def load_classification() -> Tuple[dict, dict]:
    """
    This functions loads the configuration file where there are classified
    the resolution values according the image quality

    This function use singleton design to load the file once in each run

    Returns:
        classification json
        equivalence json
    """
    global __resolution_classes
    global __equivalences_classes

    if __resolution_classes and __equivalences_classes:
        return __resolution_classes, __equivalences_classes

    with open(IMAGE_QUALITY_CONF_FILE, "r") as file:
        data = json.load(file)
        __resolution_classes = data.get("classification")
        __equivalences_classes = data.get("img_qu_equivalences")

    return __resolution_classes, __equivalences_classes


def get_resolution(qa: str) -> Tuple[str, Dict, str]:
    """
    Get the resolution value in pixels from image quality

    Args:
        qa: image quality to analise

    Returns:
        resolution
    """

    global __resolution_classes

    resolution_def = {
        "resolution": None,
        "resolution_values": {
            "X": None,
            "Y": None
        }
    }

    if not __resolution_classes:
        load_classification()

    resolution_data = __resolution_classes.get(qa, resolution_def)

    resolution = resolution_data.get("resolution")
    resolution_values = resolution_data.get("resolution_values")

    return resolution, resolution_values


def calculate_img_qa(resolution: str) -> str:
    """
    Get image quality from resolution comparing it with img quality
    classification

    Args:
        resolution: display resolution

    Returns:

    """
    global __resolution_classes

    if not __resolution_classes:
        load_classification()

    for qa, resolution_conf in __resolution_classes.items():
        if resolution_conf.get("resolution") == resolution:
            return qa
    return None


def get_equivalence(qa: str) -> str:
    """
    Get the equivalence name of a image quality. Example:

        get_equivalence("FULL HD") --> FHD
        get_equivalence("FULLHD") --> FHD

    Args:
        qa: image quality to get the equivalence

    Returns:
        equivalence value
    """

    global __equivalences_classes

    if not __equivalences_classes:
        load_classification()

    qa_eq = qa
    if " " in qa:
        qa = qa.replace(" ", "")

    return __equivalences_classes.get(qa_eq.upper(), qa)


def get_image_quality(unclean_img_qa: str) -> str:
    """
    Get image quality from unclean image quality or mixed str with two clases of
    image quality.

        Examples:
            2KUltraWideQHD
            UHD4K
            UHD5K
    Args:
        qa: unclean image quality
    """
    global __resolution_classes

    if not __resolution_classes:
        load_classification()

    major = {
        "resolution": None,
        "resolution_values": {
            "X": 0,
            "Y": 0
        }
    }
    qa_to_ret = get_equivalence(unclean_img_qa)
    for qa, resolution in __resolution_classes.items():
        major_x = major.get("resolution_values").get("X")
        res_x = resolution.get("resolution_values").get("X")
        if qa in unclean_img_qa and major_x < res_x:
            qa_to_ret = qa

    return qa_to_ret
