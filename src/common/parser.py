import os
import csv
import json
import datetime
import pandas as pd
from optparse import OptionParser
from utils import generate_output
from database.models import Display, Ecommerce
from database_tools import store_sql, store_elasticsearch

OUTPUT_PATH = '/src/outputs/parsed_data'


def to_date(display):
    display['date'] = datetime.datetime.strptime(display['date'],
                                        '%Y-%m-%d %H:%M:%S.%f')
    return display

if '__main__' == __name__:

    parser = OptionParser(version="Script to parse data to other type\n"
                                  "Examples:\n"
                                  "  $ python3 parser.py --o-type json "
                                  "--d-type csv --file <filepath>")
    parser.add_option("-o", "--o-type", dest="origin_type",
                      help="Type to convert")
    parser.add_option("-d", "--d-type", dest="dest_type",
                      help="New type format")
    parser.add_option("-i", "--file", dest="file_data",
                      help="file of the data to load and parse")

    args, _ = parser.parse_args()

    if args.origin_type and args.dest_type and args.file_data:
        if args.origin_type == 'json' and args.dest_type == 'csv':
            filename = os.path.basename(args.file_data).replace(".json", ".csv")
            dest_file = os.path.join(OUTPUT_PATH, filename)
            df = pd.read_json(args.file_data)
            df.to_csv(dest_file, index=None)
        elif args.origin_type == 'csv' and args.dest_type == 'json':
            filename = os.path.basename(args.file_data).replace(".csv", ".json")
            dest_file = os.path.join(OUTPUT_PATH, filename)
            csv_dataframe = pd.read_csv(args.file_data)
            columns = csv_dataframe.columns.values.tolist()
            data_list = []
            for indice_fila, fila in csv_dataframe.iterrows():
                data = {}
                for c in columns:
                    data[c] = fila.get(c)

                data_list.append(data)
            generate_output(data=data_list, dest_file=dest_file)
        elif args.origin_type == 'json' and args.dest_type == 'sqlite':
            with open(args.file_data, 'r') as json_file:
                displays = json.load(json_file)
                data_list = []
                first = displays[0]
                e_commerce = Ecommerce(name=first.get('e_commerce'))
                for data in displays:
                    display = Display()
                    display.json_2_model(data)
                    display.e_commerce = e_commerce
                    data_list.append(display)

                store_sql(data_list, "display")
        elif args.origin_type == 'json' and args.dest_type == 'elasticsearch':
            with open(args.file_data, 'r') as json_file:
                displays = json.load(json_file)
                displays = list(map(to_date, displays))

                store_elasticsearch(displays, "displays-simulated-2")

    else:
        parser.print_help()
