#!/usr/bin/python3
# -*- coding: utf-8; mode python -*-

"""
Functions to generate accuracy report
"""

import os
import time
import json
import pandas as pd
from datetime import datetime
import constants as common_const
import pccomponentes.utils.constants as c
from database.models import Display


OUTPUT_INDENT = "    "


def parsed_accuracy_list(parsed, discarded, start_datetime: str,
                         finish_datetime: str, incomplete_file=None,
                         xpath_errors_file=None, refresh_errors_file=None,
                         minimum_data_error_file=None):
    """
    Generates a report of the stracted data by the crawler

    Args:
        parsed: data that has been parsed
        discarded: data that has been discarded
        start_time: execution start time
        finish_time: execution finish time
        incomplete_file: output file of incomplete displays
        xpath_errors_file: output file of displays tah has benn parsed with
                           wrong specifications xpath
        refresh_errors_file: output file of display that does not hav refresh
                             time
        minimum_data_error_file: output file of errors of minimum data to get
    """
    to_alnalyse = []
    incomplete_displays = []
    xpath_errors = []
    refresh_errors = []
    minimum_data_error = {}

    if parsed and isinstance(parsed[0], Display):
        parsed = list(map(lambda disp: disp.to_json(), parsed))

    for display in parsed:

        incomplete = False
        tmp_display = display.copy()

        if display.get("size") is None \
                and display.get("resolution") is None \
                and display.get("response_time") is None \
                and display.get("refresh_speed") is None:
            xpath_errors.append(display)

        if display.get("size") is None:
            tmp_display["size"] = 0
            incomplete = True
            minimum_data_error[display['title']] = display
        else:
            tmp_display["size"] = 1

        if display.get("resolution") is None:
            tmp_display["resolution"] = 0
            incomplete = True
            minimum_data_error[display['title']] = display
        else:
            tmp_display["resolution"] = 1

        if display.get("response_time") is None:
            tmp_display["response_time"] = 0
            incomplete = True
        else:
            tmp_display["response_time"] = 1

        if display.get("refresh_speed") is None:
            tmp_display["refresh_speed"] = 0
            refresh_errors.append(display)
        else:
            tmp_display["refresh_speed"] = 1

        if incomplete:
            incomplete_displays.append(display)

        to_alnalyse.append(tmp_display)

    data_frame = pd.DataFrame.from_records(to_alnalyse)
    got_display_percent = 100 * (
            data_frame["size"].sum() / len(to_alnalyse)
    )
    got_resolution_percent = 100 * (
            data_frame["resolution"].sum() / len(to_alnalyse)
    )
    got_response_time_percent = 100 * (
            data_frame["response_time"].sum() / len(to_alnalyse)
    )
    got_refresh_speed_percent = 100 * (
            data_frame["refresh_speed"].sum() / len(to_alnalyse)
    )

    output_filename = "pcCompnetsCrawler_%s_%s.log" % (
        time.strftime("%Y%m%d%Hh%Mm%Ss"), "accuracy"
    )

    output_file_path = os.path.join(
        common_const.OUTPUT_RESULTS_PATH, "%s/accuraccy" % c.CRAWLER)

    errors_file_path = os.path.join(
        common_const.OUTPUT_RESULTS_PATH, "%s/errors" % c.CRAWLER)

    if not os.path.isdir(output_file_path):
        os.mkdir(output_file_path)

    if not os.path.isdir(errors_file_path):
        os.mkdir(errors_file_path)

    with open(os.path.join(output_file_path, output_filename), 'w') as file:
        file.write("\n{}======================================================="
                   "=\n".format(OUTPUT_INDENT))
        file.write("              Statistics of obtained information\n")
        file.write("{}======================================================="
                   "=\n".format(OUTPUT_INDENT))

        file.write(f"\n{OUTPUT_INDENT}-----------------------------------\n")
        file.write(f"{OUTPUT_INDENT}Start time: {start_datetime}\n")
        file.write(f"{OUTPUT_INDENT}Finish time: {finish_datetime}\n")
        file.write(f"{OUTPUT_INDENT}-----------------------------------\n")

        file.write("{}Total display information retrieved: {}\n".format(
            OUTPUT_INDENT, len(to_alnalyse))
        )
        file.write("{}Total display with wrong xpath: {}\n".format(
            OUTPUT_INDENT, len(xpath_errors))
        )
        file.write("{}Total display incomplete information: {}\n".format(
            OUTPUT_INDENT, len(incomplete_displays))
        )
        file.write("{}Discarded reconditioned displays: {}\n\n\n".format(
            OUTPUT_INDENT, discarded)
        )

        file.write("{}Total display size information "
                   "retrieved: {}\n".format(OUTPUT_INDENT,
                                            data_frame["size"].sum()))
        file.write("{}Total display resolution information "
                   "retrieved: {}\n".format(OUTPUT_INDENT,
                                            data_frame["resolution"].sum()))
        file.write("{}Total display response_time information "
                   "retrieved: {}\n".format(OUTPUT_INDENT,
                                            data_frame["response_time"].sum()))
        file.write("{}Total display refresh_speed information "
                   "retrieved: {}\n"
                   "\n\n".format(OUTPUT_INDENT,
                                 data_frame["refresh_speed"].sum()))

        file.write("{}Display % obtained "
                   "correct: {:.2f}%\n".format(
                        OUTPUT_INDENT, got_display_percent))
        file.write("{}Resolution % obtained correct: "
                   "{:.2f}%\n".format(OUTPUT_INDENT, got_resolution_percent))
        file.write("{}Response % obtained correct: "
                   "{:.2f}%\n".format(OUTPUT_INDENT, got_response_time_percent))
        file.write("{}Refresh % obtained correct: "
                   "{:.2f}%\n".format(OUTPUT_INDENT, got_refresh_speed_percent))

    if not incomplete_file:
        incomplete_filename = "pcCompnetsCrawler_%s_%s.log" % (
            time.strftime("%Y%m%d%Hh%Mm%Ss"), "incomplete"
        )
        incomplete_file = os.path.join(errors_file_path, incomplete_filename)
    if incomplete_displays:
        with open(incomplete_file, 'w') as file:
            file.write(json.dumps(incomplete_displays, indent=4))

    if not xpath_errors_file:
        xpath_errors_filename = "pcCompnetsCrawler_%s_%s.log" % (
            time.strftime("%Y%m%d%Hh%Mm%Ss"), "xpathERR"
        )
        xpath_errors_file = os.path.join(
            errors_file_path, xpath_errors_filename)
    if xpath_errors:
        with open(xpath_errors_file, "w") as file:
            file.write(json.dumps(xpath_errors, indent=4))

    if not refresh_errors_file:
        refresh_errors_filename = "pcCompnetsCrawler_%s_%s.log" % (
            time.strftime("%Y%m%d%Hh%Mm%Ss"), "refreshERR"
        )
        refresh_errors_file = os.path.join(
            errors_file_path, refresh_errors_filename)
    if refresh_errors:
        with open(refresh_errors_file, "w") as file:
            file.write(json.dumps(refresh_errors, indent=4))

    if not minimum_data_error_file:
        minimum_data_error_filename = "pcCompnetsCrawler_%s_%s.log" % (
            time.strftime("%Y%m%d%Hh%Mm%Ss"), "minDataERR"
        )
        minimum_data_error_file = os.path.join(
            errors_file_path, minimum_data_error_filename)

    if minimum_data_error:
        with open(minimum_data_error_file, "w") as file:
            file.write(json.dumps(minimum_data_error, indent=4))


def parsed_accuracy(parsed, incomplete_file=None, xpath_errors_file=None,
                    refresh_errors_file=None, input_name=None,
                    minimum_data_error_file=None,
                    incomplete_urls_file=None):
    """
    Generates a report of result file

    Args:
        incomplete_urls_file: generates a report file with the urls of
                              incompleted displays
        parsed: data that has been parsed
        input_name: result file to analise
        incomplete_file: output file of incomplete displays
        xpath_errors_file: output file of displays tah has benn parsed with
                           wrong specifications xpath
        refresh_errors_file: output file of display that does not hav refresh
                             time
        minimum_data_error_file: output file of errors of minimum data to get
    """

    to_alnalyse = []
    incomplete_displays = []
    incomplete_size = []
    incomplete_urls = []
    xpath_errors = []
    refresh_errors = []
    minimum_data_error = {}

    if parsed and isinstance(parsed[0], Display):
        parsed = list(map(lambda disp: disp.to_json(), parsed))

    for display in parsed:
        incomplete = False
        tmp_display = display.copy()

        if display.get("size") is None \
                and display.get("resolution") is None \
                and display.get("response_time") is None \
                and display.get("refresh_speed") is None:
            xpath_errors.append(display)

        if display.get("size") is None:
            tmp_display["size"] = 0
            incomplete = True
            minimum_data_error[display['title']] = display
            incomplete_size.append(display)
            incomplete_urls.append(display.get('url'))

        else:
            tmp_display["size"] = 1

        if display.get("resolution") is None:
            tmp_display["resolution"] = 0
            incomplete = True
            minimum_data_error[display['title']] = display
        else:
            tmp_display["resolution"] = 1

        if display.get("response_time") is None:
            tmp_display["response_time"] = 0
            incomplete = True
        else:
            tmp_display["response_time"] = 1

        if display.get("refresh_speed") is None:
            tmp_display["refresh_speed"] = 0
            refresh_errors.append(display)
        else:
            tmp_display["refresh_speed"] = 1

        if incomplete:
            incomplete_displays.append(display)

        to_alnalyse.append(tmp_display)

    data_frame = pd.DataFrame.from_records(to_alnalyse)
    got_display_percent = 100 * (
            data_frame["size"].sum() / len(to_alnalyse)
    )
    got_resolution_percent = 100 * (
            data_frame["resolution"].sum() / len(to_alnalyse)
    )
    got_response_time_percent = 100 * (
            data_frame["response_time"].sum() / len(to_alnalyse)
    )
    got_refresh_speed_percent = 100 * (
            data_frame["refresh_speed"].sum() / len(to_alnalyse)
    )

    output_filename = "pcCompnetsCrawler_%s_%s.log" % (
        time.strftime("%Y%m%d%Hh%Mm%Ss"), "accuracy"
    )

    output_file_path = os.path.join(
        common_const.OUTPUT_RESULTS_PATH, "%s/accuraccy" % c.CRAWLER)

    errors_file_path = os.path.join(
        common_const.OUTPUT_RESULTS_PATH, "%s/errors" % c.CRAWLER)

    if not os.path.isdir(output_file_path):
        os.mkdir(output_file_path)

    if not os.path.isdir(errors_file_path):
        os.mkdir(errors_file_path)

    with open(os.path.join(output_file_path, output_filename), 'w') as file:
        file.write("========================================================\n")
        if input_name:
            file.write("%s\n" % input_name)
        else:
            file.write("          Statistics of obtained information\n")
        file.write("========================================================\n")
        file.write("Total display information retrieved: {}\n".format(
            len(to_alnalyse))
        )
        file.write("Total display with wrong xpath: {}\n".format(
            len(xpath_errors))
        )
        file.write("Total display incomplete information: {}\n".format(
            len(incomplete_displays))
        )
        file.write("Total display with refresh speed "
                   "information: {}\n\n\n".format(
                        len(incomplete_displays)))

        file.write("Total display size information "
                   "retrieved: {}\n".format(data_frame["size"].sum()))
        file.write("Total display resolution information "
                   "retrieved: {}\n".format(data_frame["resolution"].sum()))
        file.write("Total display response_time information "
                   "retrieved: {}\n".format(data_frame["response_time"].sum()))
        file.write("Total display refresh_speed information "
                   "retrieved: {}\n"
                   "\n\n".format(data_frame["refresh_speed"].sum()))

        file.write("Display % obtained "
                   "correct: {:.2f}%\n".format(got_display_percent))
        file.write("Resolution % obtained correct: "
                   "{:.2f}%\n".format(got_resolution_percent))
        file.write("Response % obtained correct: "
                   "{:.2f}%\n".format(got_response_time_percent))
        file.write("Refresh % obtained correct: "
                   "{:.2f}%\n".format(got_refresh_speed_percent))

    if not incomplete_file:
        incomplete_filename = "pcCompnetsCrawler_%s_%s.log" % (
            time.strftime("%Y%m%d%Hh%Mm%Ss"), "incomplete"
        )
        incomplete_file = os.path.join(errors_file_path, incomplete_filename)
    if incomplete_displays:
        with open(incomplete_file, 'w') as file:
            file.write(json.dumps(incomplete_displays, indent=4))

    if not incomplete_urls_file:
        url_filename = "pcCompnetsCrawler_%s_%s.log" % (
            time.strftime("%Y%m%d%Hh%Mm%Ss"), "incomplete_urls"
        )
        incomplete_urls_file = os.path.join(errors_file_path, url_filename)
    if incomplete_urls:
        with open(incomplete_urls_file, 'w') as file:
            file.write(json.dumps(incomplete_urls, indent=4))

    if not xpath_errors_file:
        xpath_errors_filename = "pcCompnetsCrawler_%s_%s.log" % (
            time.strftime("%Y%m%d%Hh%Mm%Ss"), "xpathERR"
        )
        xpath_errors_file = os.path.join(
            errors_file_path, xpath_errors_filename)
    if xpath_errors:
        with open(xpath_errors_file, "w") as file:
            file.write(json.dumps(xpath_errors, indent=4))

    if not refresh_errors_file:
        refresh_errors_filename = "pcCompnetsCrawler_%s_%s.log" % (
            time.strftime("%Y%m%d%Hh%Mm%Ss"), "refreshERR"
        )
        refresh_errors_file = os.path.join(
            errors_file_path, refresh_errors_filename)
    if refresh_errors:
        with open(refresh_errors_file, "w") as file:
            file.write(json.dumps(refresh_errors, indent=4))

    if not minimum_data_error_file:
        minimum_data_error_filename = "pcCompnetsCrawler_%s_%s.log" % (
            time.strftime("%Y%m%d%Hh%Mm%Ss"), "minDataERR"
        )
        minimum_data_error_file = os.path.join(
            errors_file_path, minimum_data_error_filename)

    if minimum_data_error:
        with open(minimum_data_error_file, "w") as file:
            file.write(json.dumps(minimum_data_error, indent=4))

    with open('/src/outputs/size_errors.json',
              "w") as file:
        file.write(json.dumps(incomplete_size, indent=4))
