#!/usr/bin/python3
# -*- coding: utf-8; mode python -*-
"""
Logic to parse data of PcComponentes display URL
"""

import os
from logging import Logger
from scrapy.selector import Selector
from typing import List, Union, Dict
import pccomponentes.utils.constants as c
from pccomponentes.utils.specifications_tags import get_data
from image_quality_manager import calculate_img_qa

DRIVER_PATH = '/crawlers/chromedriver_80'
PCCOMPONENTES_ROOT_PAGE = 'https://www.pccomponentes.com'
PCCOMPONENTES_DISPLAY_PAGE = 'monitores-pc/'

XPATH_URLS = '//*[@id="articleListContent"]/div/div/article/div[1]/a/@href'
XPATH_NUM_ARTICLES = '//*[@id="totalArticles"]/text()'
XPATH_DISPLAY_IDS = '//*[@id="articleListContent"]/div/div/article/@data-id'


XPATH_DISPLAY_NAME = '//*[@id="contenedor-principal"]/div[2]/div/div[3]' \
                     '/div/div/div[1]/h1/strong/text()'
XPATH_DISPLAY_PRICE = '//*[@id="precio-main"]/@data-price'

XPATH_DISPLAY_BRAND = '//*[@id="contenedor-principal"]/div/div/div/div/div/' \
                      'div/a/text()'
XPATH_DISPLAY_PN = '//*[@id="contenedor-principal"]/div/div/div/div/div/div' \
                   '/span/text()'

XPATH_DISPLAY_SHIPPING = '//*[@id="contenedor-principal"]/div[2]/div/div[4]/' \
                         'div/div[3]/div[2]/text()'


XPATH_BASE = '//*[@id="ficha-producto-caracteristicas"]/ul[2]/li'


def get_subspecifications(tools: dict, to_find: str) -> List:
    """
    Get a sub scpecification list of display web page

    Args:
        tools: dict {
                    'xpath': xpath base to recalculate,
                    'response': scrapyResponse
                }
        to_find: parameter to find

    Returns
        extra data gotten from page
    """
    response = tools.get('response')
    filer = '//*[text()="%s"]/ul/li/text()' % to_find
    extra_data = response.xpath(filer).extract()
    return extra_data


def get_xpath(selector: Selector) -> str:
    """
    Analyzes the display web page to extract the espicifications lists

    Args:
        selector: scrapy Selector

    Returns:
        list of display specifications
    """
    data = selector.xpath(XPATH_BASE).extract_first()
    if data:
        data = data.lower()
    if data:
        if '<li>pantalla ' == data[:13]:
            return os.path.join(XPATH_BASE, 'text()')
        elif 'exhibición' in data:
            xpath = os.path.join(XPATH_BASE.replace(
                'ul[2]', 'ul'), 'ul/li/text()'
            )
            data = selector.xpath(xpath).extract_first()
            if not data:
                xpath = xpath.replace("ul/li/ul", "ul/ul")
        elif ('>display' in data or '>pantalla' in data or '>monitor' in data) \
                and not ('displayport' in data):
            xpath = os.path.join(XPATH_BASE, 'ul/li/text()')
        else:
            xpath = os.path.join(XPATH_BASE.replace('ul[2]', 'ul'), 'text()')
            data = selector.xpath(xpath).extract_first()

            if data:
                data = data.lower()
            if '>exhibición' in data:
                xpath = os.path.join(xpath, 'ul/li/text()')
            else:
                if len(data) > 10:
                    return xpath

                xpath = os.path.join(XPATH_BASE, 'text()')
            data = selector.xpath(xpath).extract()
            if len(data) <= 9:
                xpath = os.path.join(
                    XPATH_BASE.replace('ul[2]', 'ul'), "ul/li/text()")
                '//*[@id="ficha-producto-caracteristicas"]/ul/li/text'

    else:
        xpath = os.path.join(XPATH_BASE.replace('[2]/li', ''), 'li/text()')
        data = selector.xpath(xpath).extract_first()
        if data:
            data = data.lower()
            if 'pantalla\n' == data:
                xpath = os.path.join(
                    XPATH_BASE.replace('[2]', ''), 'ul/li/text()')
        else:
            xpath = xpath.replace("ul/li/text()", "/div/ul/li/ul/li/text()")

    return xpath


def parse_specifications(specifications: list, name: str, to_recalculate: dict,
                         logger: Logger) -> Dict:
    """
    This functions parses the specifications list to get display data

    Args:
        specifications: specifications list to analyze and get data
        name: display name/title
        to_recalculate: recalculate info if it is need
        logger: logger of program

    Returns:
        dict of parsed specifications
    """
    to_parse_list = ["size", "resolution", "response_time", "refresh_speed"]

    parsed = {
        "size": None,
        "resolution": None,
        "response_time": None,
        "refresh_speed": None
    }
    for to_parse in to_parse_list:
        for item in specifications:
            if item.lower() in c.SUB_ITEMS_TAGS:
                sub_specifications = get_subspecifications(to_recalculate, item)
                specifications.remove(item)
                for sub in sub_specifications:
                    if not (sub in specifications):
                        specifications.append(sub)
                continue

            data = get_data(item.lower(), to_parse, logger, name)
            parsed_data = parsed.get(to_parse, None)

            if not parsed_data and data:
                if to_parse == 'resolution':
                    parsed["resolution"] = data
                    if 'x' in data.lower():
                        values = data.lower().split('x')
                        parsed["resolution_values"] = {
                            'X': float(values[0]),
                            'Y': float(values[1])
                        }
                parsed[to_parse] = data
                break

    return parsed


def get_model(title: str, size: int, brand: str,
              logger: Logger) -> Union[str, None]:
    """
    Get the model of the display form title using size and brand

    Args:
        title: title of the display
        size: size of the display
        brand: brand of the display
        logger: logger of program

    Returns:
        model or none
    """
    try:

        if str(int(size)) in title:
            splitted = title.split(f" {int(size)}")[0]
        else:
            splitted = title.split(f" {int(round(size))}")[0]
        model = splitted.upper().replace(f"{brand.upper()} ", "")
        return model
    except Exception as e:
        logger.error(f"ERROR getting model: {title}")
    return None


def parse_display(response: Selector, logger: Logger) -> Union[Dict, None]:
    """
    get data from display web page analyzing the HTML

    Args:
        response: scrapy response selector
        logger: logger of program

    Returns:
        parsed data in a dict
    """
    name = response.xpath(c.XPATH_DISPLAY_NAME).extract_first()
    if '<strong>' in name:
        name = response.xpath(os.path.join(
            c.XPATH_DISPLAY_NAME, 'strong/text()')).extract_first()
    else:
        name = response.xpath(os.path.join(
            c.XPATH_DISPLAY_NAME, 'text()')).extract_first()

    if 'visera' in name:
        return None

    str_price = response.xpath(c.XPATH_DISPLAY_PRICE).extract_first()
    if str_price:
        price = float(str_price)
    else:
        price = "Not Available"

    brand = response.xpath(c.XPATH_DISPLAY_BRAND).extract_first()
    p_n = response.xpath(c.XPATH_DISPLAY_PN).extract_first()

    shipping = response.xpath(c.XPATH_DISPLAY_SHIPPING).extract_first()
    if shipping:
        shipping = shipping.replace('\n', '')

    xpath = get_xpath(response)
    to_recalculate = {'xpath': xpath, 'response': response}
    spec_retrieved = response.xpath(xpath).extract()
    specifications = parse_specifications(
        spec_retrieved, name, to_recalculate, logger
    )
    image_quality = calculate_img_qa(specifications.get("resolution"))
    model = get_model(
        title=name, size=specifications.get('size'), brand=brand, logger=logger
    )

    data = {
        'name': name,
        'price': price,
        'brand': brand,
        'image_quality': image_quality,
        'p_n': p_n,
        'shipping': shipping,
        'model': model
    }
    data.update(specifications)

    return data
