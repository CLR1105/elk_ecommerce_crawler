#!/usr/bin/python3
# -*- coding: utf-8; mode python -*-
"""
Constants of PcComponentes Standalone Crawler
"""


CRAWLER = 'pc_componentes'
CRAWLER_CAMELCASE = 'PcComponentes'

################################### [xpath] ####################################

PCCOMPONENTES_ROOT_PAGE = 'https://www.pccomponentes.com'
PCCOMPONENTES_DISPLAY_PAGE = 'monitores-pc/'

XPATH_URLS = '//*[@id="articleListContent"]/div/div/article/div[1]/a/@href'
XPATH_NUM_ARTICLES = '//*[@id="totalArticles"]/text()'
XPATH_DISPLAY_IDS = '//*[@id="articleListContent"]/div/div/article/@data-id'


XPATH_DISPLAY_NAME = '//*[@id="contenedor-principal"]/div[2]/div/div[3]/div/' \
                     'div/div[1]/h1'
XPATH_DISPLAY_PRICE = '//*[@id="precio-main"]/@data-price'

XPATH_DISPLAY_BRAND = '//*[@id="contenedor-principal"]/div/div/div/div/div/' \
                      'div/a/text()'
XPATH_DISPLAY_PN = '//*[@id="contenedor-principal"]/div/div/div/div/div/div' \
                   '/span/text()'

XPATH_DISPLAY_SHIPPING = '//*[@id="contenedor-principal"]/div[2]/div/div[4]/' \
                         'div/div[3]/div[2]/text()'

XPATH_DISPLAY_SPECIFICATIONS = '//*[@id="ficha-producto-caracteristicas"]' \
                               '/ul/li/ul/li/text()'


############################### [sub menus] ####################################
SUB_ITEMS_TAGS = [
    "resolución \n",
    "señal de frecuencia digital\n",
    "vídeo",
    "exhibición\t",
    "general\n",
    "resolución\n",
    "display",
    "calidad de imagen "
]