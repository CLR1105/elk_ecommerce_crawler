# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy.utils.project import get_project_settings
from pymongo import MongoClient

class MongoDBPipeline(object):
    settings = get_project_settings()

    def __init__(self):
        connection = MongoClient(
            host=self.settings['MONGODB_SERVER'],
            port=self.settings['MONGODB_PORT'],
            username=self.settings['MONGODB_USER'],
            password=self.settings['MONGODB_PASSWORD'],
            authSource=self.settings['MONGODB_DB']
        )
        db = connection[self.settings['MONGODB_DB']]
        self.collection = db[self.settings['MONGODB_COLLECTION']]

    def process_item(self, item, spider):
        self.collection.insert(dict(item))
        return item

