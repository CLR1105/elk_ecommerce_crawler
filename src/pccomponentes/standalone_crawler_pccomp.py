#!/usr/bin/python3
# -*- coding: utf-8; mode python -*-
"""
Standalone Crawler of PcComponentes displays created using Scrapy and Selenium
"""

import os
import time
from time import sleep
from logging import Logger
from logger import get_logger
from typing import List, Union
from selenium import webdriver
from utils import get_ecommerce, get_driver
import constants as common_const
from optparse import OptionParser
from utils import generate_output
from scrapy.selector import Selector
import pccomponentes.utils.constants as c
from database.models import Display, Ecommerce
from database_tools_deprecated import store_mongo
from pccomponentes.utils.page_data import parse_display
from pccomponentes.utils.report_generator import parsed_accuracy_list
from database.manager import store_elasticsearch, store_sql, \
    delete_sql_data, PYTHON_DB_DRIVERS, get_db_session


def close_iframe(driver: webdriver):
    """
    This function close the iframe to acept the terms and services

    Args:
        driver: Selenium Driver
    """
    try:
        driver.switch_to.frame(driver.find_element_by_xpath(
            '//*[@class="cn_modal_iframe"]')
        )
        driver.find_element_by_xpath('/html/body/div/a').click()
    except:
        pass


def process_urls_list(urls: list, logger: Logger, e_commerce: Ecommerce,
                      driver: webdriver = None) -> [List, List]:
    """
    This function extract the information of list of urls

    Args:
        urls: urls list
        database: database to get ecommerce, if it is None, then the Ecommerce
                  object is created as default
        logger: logger of the program
        driver: selenium driver

    Returns:
        list of the extracted data and a list of urls of discarded data
    """
    specifications_list = []
    discarded = 0
    if not driver:
        driver = get_driver()

    for url in urls:
        driver.get(url)
        sleep(0.5)
        try:
            cookies_buton = driver.find_element_by_xpath(
                '//*[text()="ACEPTAR"]'
            )

            if cookies_buton:
                cookies_buton.click()
        except Exception as e:
            pass

        close_iframe(driver)

        html = driver.page_source
        html = html.replace("<strong>", "")
        html = html.replace("</strong>", "")

        sel = Selector(text=html)

        sleep(0.2)
        data = parse_display(sel, logger)
        if data:
            data.update({'url': url})
            display = Display()
            display.json_2_model(data)
            display.e_commerce = e_commerce
            specifications_list.append(display)
        else:
            discarded += 1

    return specifications_list, discarded


def crawl_all(logger: Logger, database: str = None,
              num_pages: int = None) -> List:
    """
    Get all urls of the displays from main page
    Args:
        logger: logger of the program
        num_pages: number of display pages to extract (maybe set or not)
    Returns:
        list of extracted data
    """
    driver = get_driver()
    start_datetime = time.strftime("%Hh:%Mm:%Ss %d/%m/%Y")

    try:
        driver.get(os.path.join(
            c.PCCOMPONENTES_ROOT_PAGE, c.PCCOMPONENTES_DISPLAY_PAGE)
        )
        sleep(2)
        try:
            cookies_buton = driver.find_element_by_xpath(
                '//*[text()="ACEPTAR"]'
            )

            if cookies_buton:
                cookies_buton.click()
        except Exception as e:
            pass

        close_iframe(driver)

        sel = Selector(text=driver.page_source)
        num_displays = int(sel.xpath(c.XPATH_NUM_ARTICLES).extract_first())
        displays_ids = []
        previous_id = 0

        driver.find_element_by_xpath(
            '//*[@id="btnMore"]'
        ).click()

        while len(displays_ids) < num_displays:
            sel = Selector(text=driver.page_source)
            displays_ids = sel.xpath(c.XPATH_DISPLAY_IDS).extract()
            last_id = displays_ids[-1]

            if previous_id == last_id:
                driver.execute_script("window.scrollTo(0, %s);" % str(
                    location.get('y') + 100))

            element = driver.find_element_by_xpath(
                '//*[@data-id="%s"]' % last_id)
            driver.execute_script("arguments[0].scrollIntoView(false);",
                                  element)
            location = element.location
            previous_id = last_id

            if num_pages and len(displays_ids) > num_pages:
                break

        relative_urls = sel.xpath(
            '//*[@id="articleListContent"]/div/div/article/div[1]/a/@href'
        ).extract()

        urls = list(
            map(
                lambda url: os.path.join(
                    c.PCCOMPONENTES_ROOT_PAGE, url.replace('/', '')),
                relative_urls
            )
        )
        if num_pages and len(urls) > num_pages:
            urls = urls[:num_pages]

        generate_output(urls, "urls", c.CRAWLER)

        e_commerce = get_ecommerce(
            ecommerce_name=c.CRAWLER_CAMELCASE, database=database
        )

        specifications_list, discarded = process_urls_list(
            urls=urls, logger=logger, driver=driver, e_commerce=e_commerce
        )
        finish_datetime = time.strftime("%Hh:%Mm:%Ss %d/%m/%Y")
        parsed_accuracy_list(specifications_list, discarded,
                             start_datetime, finish_datetime)

    finally:
        driver.close()

    return specifications_list


def crawl_single_page(url: str, logger: Logger,
                      database: str = None) -> Union[Display, None]:
    """
    Crawl a single display

    Args:
        url: display url to crawl
        logger: program logger
        database: database to get ecommerce, if it is None, then the Ecommerce
                  object is created as default
    Returns:
        display data
    """

    driver = get_driver()

    driver.get(url)

    sel = Selector(text=driver.page_source)

    data = parse_display(sel, logger)
    if data:
        e_commerce = get_ecommerce(
            ecommerce_name=c.CRAWLER_CAMELCASE, database=database
        )
        data['url'] = url
        display = Display()
        display.json_2_model(data)
        display.e_commerce = e_commerce
        data = display

    driver.close()

    return data


if '__main__' == __name__:
    enum_db_list = "".join(f"{db}, " for db in PYTHON_DB_DRIVERS.keys())
    enum_db_list = enum_db_list[:-2]

    parser = OptionParser(version="Standalone crawler of MediaMarkt displays")

    parser.add_option("-s", "--crawl-single-page", dest="single_page",
                      help="Crawl a simple page")

    parser.add_option("-n", "--crawl-some-pages", dest="num_pages",
                      help="Crawl a specified number of pages")

    parser.add_option("-c", "--crawl-all", action="store_true",
                      default=False, dest="crawl_all",
                      help="Crawl all displays")

    parser.add_option("-o", "--output", dest="output_file",
                      help="Run refresh errors")

    parser.add_option("-d", "--database", dest="database",
                      help=f"Database to store de data. "
                           f"Options: {enum_db_list}.")

    parser.add_option("-C", "--collection", dest="collection",
                      help="collection or table to store in database")

    parser.add_option("--delete-prev", action="store_true",
                      default=False, dest="delete_prev",
                      help="Delete previous data of the database")

    args, _ = parser.parse_args()

    if args.collection:
        collection = args.collection
    else:
        collection = "displays"

    if args.database and not args.database in PYTHON_DB_DRIVERS:
        print("The selected database is not allowed. Pease select one of "
              "the next list:")
        for db in PYTHON_DB_DRIVERS.keys():
            print("   -", db)
    else:
        if args.delete_prev:
            delete_sql_data(database=args.database)

        if args.single_page:
            logger = get_logger("crawlSinglePage", c.CRAWLER, 2)
            result = crawl_single_page(
                url=args.single_page, logger=logger, database=args.database
            )
            generate_output(data=[result], crawler=c.CRAWLER,
                            data_type="displays", dest_file=args.output_file)

            if args.database == "elasticsearch":
                store_elasticsearch([result], collection)
            elif args.database == "mongodb":
                store_mongo([result], collection)
            elif args.database:
                sqlite_name = None
                if args.database == "sqlite":
                    sqlite_name = f"{time.strftime('%Y%m%d%Hh%Mm%Ss')}_" \
                                  f"crawler-{c.CRAWLER}.db"
                store_sql(models=[result], database=args.database,
                          sqlite_name=sqlite_name)

        elif args.num_pages:
            logger = get_logger("crawlSomePages", c.CRAWLER, 2)
            display_list = crawl_all(logger=logger,  database=args.database,
                                     num_pages=int(args.num_pages))

            generate_output(data=display_list, data_type="displays",
                            crawler=c.CRAWLER, dest_file=args.output_file)
            if args.database == "elasticsearch":
                store_elasticsearch(display_list, collection)
            elif args.database == "mongodb":
                store_mongo(display_list, collection)
            elif args.database:
                sqlite_name = None
                if args.database == "sqlite":
                    sqlite_name = f"{time.strftime('%Y%m%d%Hh%Mm%Ss')}_" \
                                  f"crawler-{c.CRAWLER}.db"
                store_sql(models=display_list, database=args.database,
                          sqlite_name=sqlite_name)

        elif args.crawl_all:
            logger = get_logger("crawlAll", c.CRAWLER, 2)
            display_list = crawl_all(logger=logger,  database=args.database)

            generate_output(data=display_list, data_type="displays",
                            crawler=c.CRAWLER, dest_file=args.output_file)
            if args.database == "elasticsearch":
                store_elasticsearch(display_list, collection)
            elif args.database == "mongodb":
                store_mongo(display_list, collection)
            elif args.database:
                sqlite_name = None
                if args.database == "sqlite":
                    sqlite_name = f"{time.strftime('%Y%m%d%Hh%Mm%Ss')}_" \
                                  f"crawler-{c.CRAWLER}.db"
                store_sql(models=display_list, database=args.database,
                          sqlite_name=sqlite_name)

        else:
            parser.print_help()