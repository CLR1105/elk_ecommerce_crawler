#!/usr/bin/python3
# -*- coding: utf-8; mode python -*-
"""
Standalone Crawler of MediaMarkt displays created using Scrapy and Selenium
"""

import os
import time
import traceback
from time import sleep
from typing import List, Union
from logging import Logger
from selenium import webdriver, common

import constants as common_const
from optparse import OptionParser
from scrapy.selector import Selector
from utils import generate_output, get_ecommerce, get_driver

import mediamarkt.utils.mediamarkt_constants as c
from mediamarkt.utils.page_data import parse_display, show_all
from mediamarkt.utils.report_generator import (
    generate_report_accuraccy
)
from database.models import Display, Ecommerce
from database_tools_deprecated import store_mongo
from database.manager import store_elasticsearch, store_sql, \
    delete_sql_data, PYTHON_DB_DRIVERS
from logger import get_logger


def close_register(driver):
    """
    This function close the register popup

    Args:
        driver: Selenium Driver
    """
    try:
        close_reg_button = driver.find_element_by_class_name("icon--closed")
        close_reg_button.click()
    except common.exceptions.TimeoutException as e:
        print(e)
        print(f"Error closing register dialog")
        print(traceback.format_exc())


def get_urls(logger: Logger, driver=None) -> List:
    """
    This function that gets the display urls from main displays web page

    Args:
        driver: Selenium Driver
        logger: Program logger

    Returns:
        list of urls
    """
    displays_url = os.path.join(
        c.MEDIAMARKT_ROOT_PAGE, c.MEDIAMARKT_DISPLAY_PAGE)

    driver.get(displays_url)
    sleep(2)
    close_register(driver)
    urls = []
    count_pages = 0
    while True:
        count_pages += 1
        sel = Selector(text=driver.page_source)
        relative_urls = sel.xpath(c.XPATH_RELATIVE_URL).extract()
        complete_urls = list(
            map(
                lambda url: os.path.join(
                    c.MEDIAMARKT_ROOT_PAGE, url[1:]
                ), relative_urls
            )
        )

        urls.extend(complete_urls)
        try:
            continue_button = driver.find_element_by_xpath(
                c.XPATH_CONTINUE_BUTTON
            )
            continue_button.click()
            sleep(4)

        except Exception as e:
            logger.debug(f"Limit reached: {count_pages}")
            break

    generate_output(urls, "urls", c.CRAWLER)
    return urls


def crawl_urls(driver: webdriver, urls: list, logger: Logger,
               e_commerce: Ecommerce) -> [List, List]:
    """
    Extract data from URLS

    Args:
        driver: Selenium webdriver
        urls: Urls to crawl and get data
        logger: Porgramm logger
        e_commerce: ecommerce crawler

    Returns:
        list of displays, list of discarded displays
    """
    if not driver:
        driver = get_driver()

    display_list = []
    thrift_displays = 0

    for url in urls:
        driver.get(url)
        sel = Selector(text=driver.page_source)

        show_all(driver)

        data = parse_display(sel, url, logger)

        if data:
            display = Display()
            display.json_2_model(data)
            display.e_commerce = e_commerce
            display_list.append(display)
        else:
            thrift_displays += 1
        sleep(1)

    return display_list, thrift_displays


def crawl_all(logger: Logger, database: str = None,
              num_pages: int = None) -> List:
    """
    Crawl all displays of MediaMarkt or set the number of pages to crawl

    Args:
        logger: Program logger
        database: database to get ecommerce, if it is None, then the Ecommerce
                  object is created as default
        num_pages: number of pages to crawl

    Returns:
        extracted data list
    """
    display_list = []
    driver = get_driver()
    start_datetime = time.strftime("%Hh:%Mm:%Ss %d/%m/%Y")

    e_commerce = get_ecommerce(
        ecommerce_name=c.CRAWLER_CAMELCASE, database=database
    )

    try:
        urls = get_urls(logger, driver)

        if num_pages and len(urls) > num_pages:
            urls = urls[:num_pages]

        display_list, thrift_displays = crawl_urls(
            driver=driver, urls=urls, logger=logger, e_commerce=e_commerce
        )
        finish_datetime = time.strftime("%Hh:%Mm:%Ss %d/%m/%Y")
        generate_report_accuraccy(display_list, thrift_displays,
                                  start_datetime, finish_datetime)

    except ValueError as e:
        logger.error(e)
    driver.close()

    return display_list


def crawl_single_page(url: str, logger: Logger,
                      database: str = None) -> Union[Display, None]:
    """
    Crawl a single display

    Args:
        url: display url to crawl
        logger: program logger
        database: database to get ecommerce, if it is None, then the Ecommerce
                  object is created as default
    Returns:
        display data
    """

    driver = get_driver()

    driver.get(url)

    sel = Selector(text=driver.page_source)

    data = parse_display(sel, url, logger)
    if data:
        e_commerce = get_ecommerce(
            ecommerce_name=c.CRAWLER_CAMELCASE, database=database
        )

        display = Display()
        display.json_2_model(data)
        display.e_commerce = e_commerce
        data = display

    driver.close()

    return data


if '__main__' == __name__:
    enum_db_list = "".join(f"{db}, " for db in PYTHON_DB_DRIVERS.keys())
    enum_db_list = enum_db_list[:-2]

    parser = OptionParser(version="Standalone crawler of MediaMarkt displays")

    parser.add_option("-s", "--crawl-single-page", dest="single_page",
                      help="Crawl a simple page")

    parser.add_option("-n", "--crawl-some-pages", dest="num_pages",
                      help="Crawl a specified number of pages")

    parser.add_option("-c", "--crawl-all", action="store_true",
                      default=False, dest="crawl_all",
                      help="Crawl all displays")

    parser.add_option("-o", "--output", dest="output_file",
                      help="Run refresh errors")

    parser.add_option("-d", "--database", dest="database",
                      help=f"Database to store de data. "
                           f"Options: {enum_db_list}.")

    parser.add_option("-C", "--collection", dest="collection",
                      help="collection or table to store in database")

    parser.add_option("--delete-prev", action="store_true",
                      default=False, dest="delete_prev",
                      help="Delete previous data of the database")

    args, _ = parser.parse_args()

    if args.collection:
        collection = args.collection
    else:
        collection = "displays"

    if args.database and not args.database in PYTHON_DB_DRIVERS:
        print("The selected database is not allowed. Pease select one of "
              "the next list:")
        for db in PYTHON_DB_DRIVERS.keys():
            print("   -", db)
    else:
        if args.single_page:
            logger = get_logger("crawlSinglePage", c.CRAWLER, 2)
            result = crawl_single_page(
                url=args.single_page, logger=logger, database=args.database
            )

            generate_output(data=[result], crawler=c.CRAWLER,
                            data_type="displays", dest_file=args.output_file)

            if args.database == "elasticsearch":
                store_elasticsearch([result], collection)
            elif args.database == "mongodb":
                store_mongo([result], collection)
            elif args.database:
                sqlite_name = None
                if args.database == "sqlite":
                    sqlite_name = f"{time.strftime('%Y%m%d%Hh%Mm%Ss')}_" \
                                  f"crawler-{c.CRAWLER}.db"
                elif args.delete_prev:
                    delete_sql_data(database=args.database)
                store_sql(models=[result], database=args.database,
                          sqlite_name=sqlite_name)

        elif args.num_pages:
            logger = get_logger("crawlSomePages", c.CRAWLER, 2)
            display_list = crawl_all(
                logger=logger,
                database=args.database,
                num_pages=int(args.num_pages)
            )
            generate_output(
                data=display_list, data_type="displays",
                crawler=c.CRAWLER, dest_file=args.output_file
            )

            if args.database == "elasticsearch":
                store_elasticsearch(display_list, collection)
            elif args.database == "mongodb":
                store_mongo(display_list, collection)
            elif args.database:
                sqlite_name = None
                if args.database == "sqlite":
                    sqlite_name = f"{time.strftime('%Y%m%d%Hh%Mm%Ss')}_" \
                                  f"crawler-{c.CRAWLER}.db"
                elif args.delete_prev:
                    delete_sql_data(database=args.database)
                store_sql(models=display_list, database=args.database,
                          sqlite_name=sqlite_name)

        elif args.crawl_all:
            logger = get_logger("crawlAll", c.CRAWLER, 2)
            display_list = crawl_all(
                logger=logger,
                database=args.database
            )
            generate_output(
                data=display_list, data_type="displays",
                crawler=c.CRAWLER, dest_file=args.output_file
            )

            if args.database == "elasticsearch":
                store_elasticsearch(display_list, collection)
            elif args.database == "mongodb":
                store_mongo(display_list, collection)
            elif args.database:
                sqlite_name = None
                if args.database == "sqlite":
                    sqlite_name = f"{time.strftime('%Y%m%d%Hh%Mm%Ss')}_" \
                                  f"crawler-{c.CRAWLER}.db"
                elif args.delete_prev:
                    delete_sql_data(database=args.database)
                store_sql(models=display_list, database=args.database,
                          sqlite_name=sqlite_name)

        elif args.database and args.delete_prev:
            delete_sql_data(args.database)

        else:
            parser.print_help()
