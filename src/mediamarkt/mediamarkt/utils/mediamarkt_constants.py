#!/usr/bin/python3
# -*- coding: utf-8; mode python -*-
"""
Constants of MediaMarkt Standalone Crawler
"""

CRAWLER = 'mediamarkt'
CRAWLER_CAMELCASE = 'MediaMarkt'

MEDIAMARKT_ROOT_PAGE = 'https://www.mediamarkt.es'
MEDIAMARKT_DISPLAY_PAGE = 'es/category/_todos-los-monitores-701438.html'

XPATH_CONTINUE_BUTTON = '//*[text()="Continuar"]'

XPATH_CLOSE_REGISTER = '//*[@id="rise-header"]/div/div/div[4]/div/div[2]/div/a'

XPATH_RELATIVE_URL = '//*[@id="category"]/ul/li/div/div/h2/a/@href'

XPATH_PRODUCT_INFO = '//dt[text()="%s"]/following-sibling::dd/text()'

XPATH_HEADER_NAME = '//*[@id="product-details"]/div/h1/text()'
XPATH_PRICE = '//*[@id="product-details"]/div[2]/div[1]/div[2]/text()'
XPATH_PRICE_2 = '//*[@itemprop="price"]/@content'
XPATH_PRODUCT_NUMBER = '//*[@id="product-details"]/div/div/dl/dd[' \
                       '1]/span/text()'

XPATH_MORE_BUTTON = '//*[@id="pdp_default_configuration-content_' \
                    'product_detail"]/div/div/a'


