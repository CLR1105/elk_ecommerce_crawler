#!/usr/bin/python3
# -*- coding: utf-8; mode python -*-

"""
Functions to generate accuracy report
"""

import os
import time
from datetime import datetime
from io import TextIOWrapper

import pandas as pd
import constants as common_const
import mediamarkt.utils.mediamarkt_constants as c
from database.models import Display


OUTPUT_INDENT = "    "


def dump_to_file(display: dict, file: TextIOWrapper):
    """
    Function that dup to file a report info of a display

    Args:
        display: diplay to report
        file: file to write in
    """
    name = display.get("title")
    brand = display.get("brand")
    model = display.get("model")
    url = display.get("url")
    indent = OUTPUT_INDENT + OUTPUT_INDENT
    file.write("\n{}- name: {}\n".format(indent, name))
    file.write("{}  model: {}\n".format(indent, model))
    file.write("{}  brand: {}\n".format(indent, brand))
    file.write("{}  url: {}\n".format(indent, url))


def generate_report_accuraccy(data: list, thrift_displays: list,
                              start_time: str, finish_time: str):
    """
    Generates a accuracy report that evaluates all extracted or not data and
    analyse the discarded displays

    Args:
        start_time: execution start time
        finish_time: execution finish time
        data: extracted data to analyse
        thrift_displays: discarded displays
    """
    to_alnalyse = []
    incomplete_displays = []
    displays_without_price = []
    displays_without_size = []
    displays_without_resolution = []
    displays_without_image_quality = []
    displays_without_title = []
    displays_without_brand = []
    displays_without_response_time = []
    displays_without_refresh_speed = []

    if data and isinstance(data[0], Display):
        data = list(map(lambda disp: disp.to_json(), data))

    for display in data:
        incomplete = False
        tmp_display = display.copy()

        if display.get("title") is None:
            tmp_display["title"] = 0
            incomplete = True
            displays_without_title.append(display)
        else:
            tmp_display["title"] = 1

        if display.get("price") is None:
            tmp_display["price"] = 0
            incomplete = True
            displays_without_price.append(display)
        else:
            tmp_display["price"] = 1

        if display.get("image_quality") is None:
            tmp_display["image_quality"] = 0
            incomplete = True
            displays_without_image_quality.append(display)
        else:
            tmp_display["image_quality"] = 1

        if display.get("brand") is None:
            tmp_display["brand"] = 0
            incomplete = True
            displays_without_brand.append(display)
        else:
            tmp_display["brand"] = 1

        if display.get("model") is None:
            tmp_display["model"] = 0
            incomplete = True
        else:
            tmp_display["model"] = 1

        if display.get("size") is None:
            tmp_display["size"] = 0
            incomplete = True
            displays_without_size.append(display)
        else:
            tmp_display["size"] = 1

        if display.get("resolution") is None:
            tmp_display["resolution"] = 0
            incomplete = True
            displays_without_resolution.append(display)
        else:
            tmp_display["resolution"] = 1

        if display.get("response_time") is None:
            tmp_display["response_time"] = 0
            incomplete = True
            displays_without_response_time.append(display)
        else:
            tmp_display["response_time"] = 1

        if display.get("refresh_speed") is None:
            tmp_display["refresh_speed"] = 0
            displays_without_refresh_speed.append(display)
        else:
            tmp_display["refresh_speed"] = 1

        if incomplete:
            incomplete_displays.append(display)

        to_alnalyse.append(tmp_display)

    data_frame = pd.DataFrame.from_records(to_alnalyse)

    total_size = data_frame["size"].sum()
    total_price = data_frame["price"].sum()
    total_resolution = data_frame["resolution"].sum()
    total_img_qa = data_frame["image_quality"].sum()
    total_response_time = data_frame["response_time"].sum()
    total_refresh_speed = data_frame["refresh_speed"].sum()
    total_title = data_frame["title"].sum()
    total_model = data_frame["model"].sum()
    total_brand = data_frame["brand"].sum()

    got_size_percent = round(100 * (total_size / len(to_alnalyse)), 2)
    got_price_percent = round(100 * (total_price / len(to_alnalyse)), 2)
    got_title_percent = round(100 * (total_title / len(to_alnalyse)), 2)
    got_model_percent = round(100 * (total_model / len(to_alnalyse)), 2)
    got_brand_percent = round(100 * (total_brand / len(to_alnalyse)), 2)
    got_resolution_percent = round(
        100 * (total_resolution / len(to_alnalyse)), 2)
    got_img_qa_percent = round(100 * (total_img_qa / len(to_alnalyse)), 2)
    got_response_time_percent = round(
        100 * (total_response_time / len(to_alnalyse)), 2)
    got_refresh_speed_percent = round(
        100 * (total_refresh_speed / len(to_alnalyse)), 2)

    output_filename = "mediamarktCrawler_%s_%s.log" % (
        time.strftime("%Y%m%d%Hh%Mm%Ss"), "accuracy"
    )

    output_file_path = os.path.join(
        common_const.OUTPUT_RESULTS_PATH, "%s/accuraccy" % c.CRAWLER)

    if not os.path.isdir(output_file_path):
        os.mkdir(output_file_path)

    with open(os.path.join(output_file_path, output_filename), 'w') as file:
        file.write(f"\n{OUTPUT_INDENT}========================================="
                   f"===============\n")
        file.write("              Statistics of obtained information")
        file.write(f"\n{OUTPUT_INDENT}========================================="
                   f"===============\n")

        file.write(f"\n{OUTPUT_INDENT}-----------------------------------\n")
        file.write(f"{OUTPUT_INDENT}Start time: {start_time}\n")
        file.write(f"{OUTPUT_INDENT}Finish time: {finish_time}\n")
        file.write(f"{OUTPUT_INDENT}-----------------------------------\n")

        file.write(f"{OUTPUT_INDENT}Total display information retrieved: "
                   f"{len(to_alnalyse)}\n")
        file.write(f"{OUTPUT_INDENT}Total display incomplete information: "
                   f"{len(incomplete_displays)}\n")
        file.write(f"{OUTPUT_INDENT}Discarded reconditioned displays: "
                   f"{thrift_displays}\n\n\n")

        file.write(f"{OUTPUT_INDENT}Total display name information retrieved: "
                   f"{total_title}\n")
        file.write(f"{OUTPUT_INDENT}Total display brand information retrieved: "
                   f"{total_brand}\n")
        file.write(f"{OUTPUT_INDENT}Total display model information retrieved: "
                   f"{total_model}\n")
        file.write(f"{OUTPUT_INDENT}Total display size information retrieved: "
                   f"{total_size}\n")
        file.write(f"{OUTPUT_INDENT}Total display image quality information "
                   f"retrieved: {total_img_qa}\n")
        file.write(f"{OUTPUT_INDENT}Total display price information retrieved: "
                   f"{total_price}\n")
        file.write(f"{OUTPUT_INDENT}Total display resolution information "
                   f"retrieved: {total_resolution}\n")
        file.write(f"{OUTPUT_INDENT}Total display response_time information "
                   f"retrieved: {total_response_time}\n")
        file.write(f"{OUTPUT_INDENT}Total display refresh_speed information "
                   f"retrieved: {total_refresh_speed}\n\n\n")

        file.write(f"{OUTPUT_INDENT}Display title % obtained correct: "
                   f"{got_title_percent}%\n")
        file.write(f"{OUTPUT_INDENT}Display model % obtained correct: "
                   f"{got_model_percent}%\n")
        file.write(f"{OUTPUT_INDENT}Display brand % obtained correct: "
                   f"{got_brand_percent}%\n")
        file.write(f"{OUTPUT_INDENT}Price % obtained correct: "
                   f"{got_price_percent}%\n")
        file.write(f"{OUTPUT_INDENT}Display size % obtained correct: "
                   f"{got_size_percent}%\n")
        file.write(f"{OUTPUT_INDENT}Display image quality % obtained correct: "
                   f"{got_img_qa_percent}%\n")
        file.write(f"{OUTPUT_INDENT}Resolution % obtained correct: "
                   f"{got_resolution_percent}%\n")
        file.write(f"{OUTPUT_INDENT}Response time % obtained correct: "
                   f"{got_response_time_percent}%\n")
        file.write(f"{OUTPUT_INDENT}Refresh % obtained correct: "
                   f"{got_refresh_speed_percent}%\n")

        if displays_without_size:
            file.write("\n\n{OUTPUT_INDENT}[Displays without size]\n")
            for display in displays_without_size:
                dump_to_file(display, file)

        if displays_without_image_quality:
            file.write(f"\n\n{OUTPUT_INDENT}[Displays without image quality]\n")
            for display in displays_without_image_quality:
                dump_to_file(display, file)

        if displays_without_resolution:
            file.write(f"\n\n{OUTPUT_INDENT}[Displays without resolution]\n")
            for display in displays_without_resolution:
                dump_to_file(display, file)

        if displays_without_price:
            file.write(f"\n\n{OUTPUT_INDENT}[Displays without price]\n")
            for display in displays_without_price:
                dump_to_file(display, file)

        if displays_without_title:
            file.write(f"\n\n{OUTPUT_INDENT}[Displays without name]\n")
            for display in displays_without_title:
                dump_to_file(display, file)

        if displays_without_brand:
            file.write(f"\n\n{OUTPUT_INDENT}[Displays without brand]\n")
            for display in displays_without_brand:
                dump_to_file(display, file)

        if displays_without_refresh_speed:
            file.write(f"\n\n{OUTPUT_INDENT}[Displays without refresh speed]\n")
            for display in displays_without_refresh_speed:
                dump_to_file(display, file)

        if displays_without_response_time:
            file.write(f"\n\n{OUTPUT_INDENT}[Displays without response time]\n")
            for display in displays_without_response_time:
                dump_to_file(display, file)
