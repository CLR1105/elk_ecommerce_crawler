# -*- coding: utf-8 -*-
from scrapy import Spider
from scrapy.http import Request
import os
import sys
import json
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# export PYTHONPATH=$PYTHONPATH:/home/christian/repos/tfm/crawlers/demo_books_ecommerce/demo_books_ecommerce/utils

class BooksCrawlerSpider(Spider):
    name = 'books_crawler'
    allowed_domains = ['books.toscrape.com']
    start_urls = ['http://books.toscrape.com/']

    def parse(self, response):
        """
        This function parses the response if http request

        :param response: http.Response

        :return: Request of sub url
        """

        # Get all books relative urls that are in <h3><a href="url"></a></h3>
        books_url = response.xpath('//h3/a/@href').extract()

        for book_url in books_url:
            # Build the absolute url of a book
            absolute_book_url = response.urljoin(book_url)

            # Returns by yield a request of absolute book url
            yield Request(absolute_book_url, callback=self.parse_book)

        # Go to the next page of the list books
        # All tags that are like: <a href="url">next</a>
        next_page_relative_url = response.xpath(
            '//a[text()="next"]/@href'
        ).extract_first()

        # Build the absolute url of next page
        next_page_relative_url = response.urljoin(next_page_relative_url)

        # Returns by yield a request of absolute next url
        yield Request(next_page_relative_url)



    def product_info(self, response, key):
        """
        This functions gets the value of a given key of html table

        :param response: http.Response
        :param key: string that defines the key to get value

        :return: value
        """

        # <tbody>
        #     <tr>
        #       <th>key</th><td>value</td>
        #     </tr>
        #     ...
        #  </tbody>
        return response.xpath(
            '//th[text()="{}"]/following-sibling::td/text()'.format(key)
        ).extract_first()


    def parse_book(self, response):
        """
        This functions gets book data from http request

        :param response: http.Response

        :return: Dict with the books information
        """

        # <h1>Book Title</h1>
        title = response.xpath('//h1/text()').extract_first()
        title = title.replace("\u2019", "\'")
        title = title.replace("\u201c", "\'\'")
        title = title.replace("\u201d", "\'\'")
        title = title.replace("á", "a")
        title = title.replace("é", "e")
        title = title.replace("í", "i")
        title = title.replace("ó", "o")
        title = title.replace("u", "u")

        # <p class="price_color">Price of the book</p>
        price = response.xpath(
            '//*[@class="price_color"]/text()'
        ).extract_first()

        price = float(price.replace(u"\u00a3", ""))



        # <img src="../../relative/url/image.png">
        image_relative_url = response.xpath('//img/@src').extract_first()
        image_absolute_url = image_relative_url.replace(
            '../..', 'http://books.toscrape.com'
        )

        # <p class="star-rating Number">...</p>
        rating = response.xpath(
            '//*[contains(@class, "star-rating")]/@class'
        ).extract_first()
        rating = rating.replace('star-rating ', '')

        # <div id="product_description"> <h2>Product Description</h2> </div>
        # <p> This is the description of the book</p>
        description = response.xpath(
            '//*[@id="product_description"]/following-sibling::p'
        ).extract_first()

        # product information of table
        upc = self.product_info(response, 'UPC')
        product_type = self.product_info(response, 'Product Type')
        price_without_tax = self.product_info(response, 'Price (excl. tax)')
        price_with_tax = self.product_info(response, 'Price (incl. tax)')
        tax = self.product_info(response, 'Tax')
        availability = self.product_info(response, 'Availability')
        number_of_reviews = self.product_info(response, 'Number of reviews')


        yield {
            'title': title,
            'price': price,
            'image_url': image_absolute_url,
            'rating': rating,
            'upc': upc,
            'product_type': product_type,
        }




    def close(self, reason):
        #dir_path = os.path.realpath(__file__)
        #sqlalquemy_dir = os.path.join(dir_path, '../utils')
        #sys.path.insert(0, sqlalquemy_dir)

        from database.session_default import get_connection_string
        from database.models import Book, Base

        json_file = '/crawlers/' \
                    'demo_books_ecommerce/books.json'

        os.environ["CLIENT_DB_USERNAME"] = "durin"
        os.environ["CLIENT_DB_PASSWORD"] = "Balrog2020!"
        os.environ["CLIENT_DB_HOST"] = "127.0.0.1"
        os.environ["CLIENT_DB_PORT"] = "3306"
        os.environ["CLIENT_DB_NAME"] = "book_store"

        connection_conf = get_connection_string()
        engine = create_engine(connection_conf)

        Base.metadata.create_all(bind=engine)
        Session = sessionmaker(bind=engine)

        session = Session()

        with open(json_file) as f:
            books = json.load(f)

            for book_data in books:
                book = Book(
                    title=book_data.get('title'),
                    price=float(book_data.get('price')),
                    image_url=book_data.get('image_url'),
                    rating=book_data.get('rating'),
                    upc=book_data.get('upc'),
                    product_type=book_data.get('product_type'),
                )
                session.add(book)

        session.commit()
        session.close()

        #os.system('rm -f {}'.format(json_file))

