'''import database.session_default as sessionTools
import os, sys
from alembic import op

from sqlalchemy import Enum

import enum

class ProductType(enum.Enum):
    """
    ProductType enum
    """
    BASIC = 1
    SILVER = 2
    GOLD = 3
    PLATINUM = 4

    @classmethod
    def has_value(self, value: int):
        return value in self._value2member_map_

    @classmethod
    def has_name(self, name: str):
        return name in self._member_names_

    @classmethod
    def get_type_obj_name (self, name: str):
        if name == ProductType.BASIC.name:
            return ProductType.BASIC
        elif name == ProductType.SILVER.name:
            return ProductType.SILVER
        elif name == ProductType.GOLD.name:
            return ProductType.GOLD
        else:
            return ProductType.PLATINUM


if __name__ == "__main__":


    print("name: ", ProductType.has_name("BASIC"))
    print("value: ", ProductType.has_value(4))
    print("value from name: ", str(ProductType.get_type_obj_name("BASIC")))
    #sessionTools.setup_db()

    #sessionTools.create_tables()

    #sessionTools.drop_tables()


    #dir_path = os.path.dirname(os.path.realpath(__file__))
    #print (dir_path)
    #libsms_path = os.path.join(dir_path, '../../../../libsms/')
    #print(libsms_path)
    #sys.path.insert(0, libsms_path)
'''


