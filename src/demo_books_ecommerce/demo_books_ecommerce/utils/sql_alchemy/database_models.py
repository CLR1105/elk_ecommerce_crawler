from sqlalchemy import create_engine, Column, Integer, String, \
    Float, ForeignKey, Enum, Constraint, \
    Table, pool
from sqlalchemy.exc import SQLAlchemyError

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.associationproxy import association_proxy
from database.session_default import get_connection_string

#from database.session_default import get_connection_string

import os, enum, sys
import json

Base = declarative_base()


class Book(Base):
    __tablename__ = "book"

    db_id = Column(Integer, primary_key=True, unique=True, autoincrement=True)
    title = Column(String(250), primary_key=True)
    price = Column(Float)
    image_url = Column(String(1000), primary_key=True)
    rating = Column(String(10))
    upc = Column(String(20), primary_key=True)
    product_type = Column(String(50))

    def to_json(self):
        return {
            'db_id': self.db_id,
            'title': self.title,
            'price': self.price,
            'image_url': self.image_url,
            'rating': self.rating,
            'upc': self.upc,
            'product_type': self.product_type,
        }


    """
    class Product(Base):
    __tablename__ = "product"

    id = Column("id",Integer, primary_key=True, unique=True, autoincrement=True)
    type = Column("type",Enum(ProductType), nullable=True)
    owner_id = Column("owner_id",Integer, ForeignKey('user.id'))
    owner = relationship("User", back_populates="product")
    """
if __name__ == "__main__":

    engine = create_engine(
        'mysql+mysqldb://durin:Balrog2020!@127.0.0.1:3306/test_mysql',
        pool_recycle=3600
    )

    os.environ["CLIENT_DB_USERNAME"] = "durin"
    os.environ["CLIENT_DB_PASSWORD"] = "Balrog2020!"
    os.environ["CLIENT_DB_HOST"] = "127.0.0.1"
    os.environ["CLIENT_DB_PORT"] = "3306"
    os.environ["CLIENT_DB_NAME"] = "book_store"

    connection_conf = get_connection_string()
    engine = create_engine(connection_conf)


    #    '''
    #    upc="a897fe39b1053632"
    #    product_type="Books"
    #   price_tax = 51.77
    #    price_no_tax = 51.77
    #    tax = 0.00
    #    availability = "In stock (22 available)"
    #    reviews = 0
    #    '''
    #    book_description = """It's hard to imagine a world without A Light in
    #    the Attic. This now-classic collection of poetry and drawings from Shel Silverstein celebrates its 20th anniversary with this special edition. Silverstein's humorous and creative verse can amuse the dowdiest of readers. Lemon-faced adults and fidgety kids sit still and read these rhythmic words and laugh and smile and love th It's hard to imagine a world without A Light in the Attic. This now-classic collection of poetry and drawings from Shel Silverstein celebrates its 20th anniversary with this special edition. Silverstein's humorous and creative verse can amuse the dowdiest of readers. Lemon-faced adults and fidgety kids sit still and read these rhythmic words and laugh and smile and love that Silverstein. Need proof of his genius? RockabyeRockabye baby, in the treetopDon't you know a treetopIs no safe place to rock?And who put you up there,And your cradle, too?Baby, I think someone down here'sGot it in for you. Shel, you never sounded so good. ...more"""
    #
    #    book = Book(
    #        title="A Light in the Attic",
    #        price=51.77,
    #        image_url="http://books.toscrape.com/media/cache/fe/72
    #        /fe72f0532301ec28892ae79a629a293c.jpg",
    #        rating="Three",
    #        upc="a897fe39b1053632",
    #        product_type="Books",
    #    )
    #
    #    session.add(book)
    
    books_json_list=[]
    with engine.connect() as connection:
        book_data = connection.execute(
            "select DISTINCT title, price,image_url, rating, upc, product_type "
            "from book where rating = "
            "'Four' limit 10;")
        for book in book_data:
            if book:
                books_json_list.append(
                    {
                        'title': book[0],
                        'price': book[1],
                        'image_url': book[2],
                        'rating': book[3],
                        'upc': book[4],
                        'product_type': book[5],
                    }
                )

        with open('data.json', 'w') as outfile:
            json.dump(books_json_list, outfile)

